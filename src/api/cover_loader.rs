// Shortwave - cover_loader.rs
// Copyright (C) 2024  Felix Häcker <haeckerfelix@gnome.org>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::{Error, Result};
use async_channel::Sender;
use cached::proc_macro::io_cached;
use futures_util::StreamExt;
use gdk::RGBA;
use glycin::Loader;
use gtk::gio::{Cancelled, File};
use gtk::graphene::Rect;
use gtk::prelude::TextureExt;
use gtk::prelude::*;
use gtk::{gdk, gio, glib, gsk};
use url::Url;

use crate::path;

#[derive(Debug, Clone)]
struct CoverRequest {
    favicon_url: Url,
    size: i32,
    sender: Sender<Result<gdk::Texture>>,
    cancellable: gio::Cancellable,
}

#[derive(Debug, Clone)]
pub struct CoverLoader {
    request_sender: Sender<CoverRequest>,
}

impl CoverLoader {
    pub fn new() -> Self {
        let (request_sender, request_receiver) = async_channel::unbounded::<CoverRequest>();
        let request_stream = request_receiver
            .map(Self::handle_request)
            .buffer_unordered(usize::max(glib::num_processors() as usize / 2, 2));

        glib::spawn_future_local(async move {
            request_stream.collect::<Vec<_>>().await;
        });

        Self { request_sender }
    }

    pub async fn prune_cache(&self) {
        // Remove old Shortwave pre v4.0 cache
        let mut path = path::CACHE.clone();
        path.push("favicons");
        let _ = std::fs::remove_dir_all(&path);

        if let Some(c) = COVER.get() {
            let res = c.remove_expired_entries();
            debug!("Remove expired cover cache entries: {:?}", res);
        }
    }

    pub async fn load_cover(
        &mut self,
        favicon_url: &Url,
        size: i32,
        cancellable: gio::Cancellable,
    ) -> Result<gdk::Texture> {
        let (sender, receiver) = async_channel::bounded(1);

        let request = CoverRequest {
            favicon_url: favicon_url.clone(),
            size,
            sender,
            cancellable: cancellable.clone(),
        };
        self.request_sender
            .send(request)
            .await
            .map_err(|_| Error::msg("Unable to send cover request"))?;

        receiver.recv().await?
    }

    async fn handle_request(request: CoverRequest) {
        let res = gio::CancellableFuture::new(
            Self::cover_texture(&request.favicon_url, request.size),
            request.cancellable.clone(),
        )
        .await;

        let msg = match res {
            Ok(res) => res,
            Err(Cancelled) => Err(Error::msg("cancelled")),
        };

        request.sender.send(msg).await.unwrap();
    }

    async fn cover_texture(favicon_url: &Url, size: i32) -> Result<gdk::Texture> {
        let cover = cover_bytes(favicon_url, size).await?;
        let bytes = glib::Bytes::from_owned(cover);
        Ok(gdk::Texture::from_bytes(&bytes)?)
    }
}

impl Default for CoverLoader {
    fn default() -> Self {
        Self::new()
    }
}

#[io_cached(
    disk = true,
    name = "COVER",
    time = 604_800, // 1 week
    key = "String",
    convert = r#"{ format!("{}@{}", favicon_url, size) }"#,
    map_error = r#"|e| Error::msg(format!("{:?}", e))"#
)]
async fn cover_bytes(favicon_url: &Url, size: i32) -> Result<Vec<u8>> {
    let file = File::for_uri(favicon_url.as_str());

    let loader = Loader::new(file);
    let image = loader.load().await?;
    let texture = image.next_frame().await?.texture();

    let snapshot = gtk::Snapshot::new();
    snapshot_thumbnail(&snapshot, texture, size as f32);

    let node = snapshot.to_node().unwrap();
    let renderer = gsk::CairoRenderer::new();
    let display = gdk::Display::default().expect("No default display available");
    renderer.realize_for_display(&display)?;

    let rect = Rect::new(0.0, 0.0, size as f32, size as f32);
    let texture = renderer.render_texture(node, Some(&rect));
    renderer.unrealize();

    Ok(texture.save_to_png_bytes().to_vec())
}

// Ported from Highscore (Alice Mikhaylenko)
// https://gitlab.gnome.org/World/highscore/-/blob/b07460f0c1475269381902c6305e4d91e55b61f5/src/library/cover-loader.vala#L124
fn snapshot_thumbnail(snapshot: &gtk::Snapshot, cover: gdk::Texture, size: f32) {
    let aspect_ratio = cover.width() as f32 / cover.height() as f32;
    let mut width = size;
    let mut height = size;

    if aspect_ratio < 1.0 {
        width = aspect_ratio * size;
    } else {
        height = size / aspect_ratio;
    }

    if width >= size - 2.0 {
        width = size;
    }

    if height >= size - 2.0 {
        height = size;
    }

    let cover_rect = Rect::new((size - width) / 2.0, (size - height) / 2.0, width, height);

    snapshot.push_clip(&Rect::new(0.0, 0.0, size, size));

    snapshot.append_color(
        &RGBA::new(0.0, 0.0, 0.0, 1.0),
        &Rect::new(0.0, 0.0, size, size),
    );

    if width < size || height < size {
        let blur_radio = size / 4.0;

        let outer_rect_width;
        let outer_rect_height;
        if aspect_ratio < 1.0 {
            outer_rect_width = size + blur_radio * 2.0;
            outer_rect_height = outer_rect_width / aspect_ratio;
        } else {
            outer_rect_height = size + blur_radio * 2.0;
            outer_rect_width = aspect_ratio * outer_rect_height;
        }

        let outer_rect = Rect::new(
            (size - outer_rect_width) / 2.0,
            (size - outer_rect_height) / 2.0,
            outer_rect_width,
            outer_rect_height,
        );

        snapshot.push_blur(blur_radio as f64);
        snapshot.append_texture(&cover, &outer_rect);
        snapshot.pop();
        snapshot.append_color(&RGBA::new(0.0, 0.0, 0.0, 0.2), &outer_rect);
    }

    snapshot.append_scaled_texture(&cover, gsk::ScalingFilter::Trilinear, &cover_rect);
    snapshot.pop();
}
